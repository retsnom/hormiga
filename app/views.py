from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views.decorators.http import require_http_methods
from django.views.decorators.cache import never_cache
from django.core import serializers
from django.shortcuts import redirect
from app.models import Company, Project, Client, Video
import json
# Create your views here.
def index(request):
    return HttpResponse('hello')

@require_http_methods(["GET"])
def clipboard_info(request, project_id):
    if request.user.is_staff:

        try:
            project  = Project.objects.get(id=project_id)
            client = project.client
            client_firstname = client.firstname
            client_lastname = client.lastname

            video = list(Video.objects.filter(project=project).values())
            data = {
                'project_name': project.name,
                'client_firstname': client_firstname,
                'client_lastname': client_lastname,

                'video': video
                    }
            return render(request, 'clipboard_info.html', data)
        except:
            return []
    return redirect('/')

@require_http_methods(["GET"])
def lastnvideos(request):
    if request.user.is_staff:
        data = list(Video.objects.order_by('-updated')[:5].values());
        return render(request, 'last_n_videos.html', {'data': data})
    return redirect('/')