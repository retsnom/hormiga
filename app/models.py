from django.contrib.postgres.fields import JSONField
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save, pre_save , pre_save 
from django.dispatch import receiver
from datetime import timedelta

# Create your models here.

class Person(models.Model):
    id = models.AutoField(primary_key=True)
    firstname = models.CharField(max_length=50)
    lastname = models.CharField(max_length=50)
    email = models.EmailField(max_length=255)
    description = models.TextField(null=True)
    created = models.DateField(auto_now_add=True)
    updated = models.DateField(auto_now=True)

    class Meta:
        abstract = True

class Projectmanager(Person):

    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.firstname

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Projectmanager.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.projectmanager.save()

class Company(models.Model):
    name = models.CharField(max_length=256, unique=True)
    street = models.CharField(max_length=256, null=True)
    zip_code= models.CharField(max_length=256, null=True)
    place = models.CharField(max_length=256, null=True)
    country = models.CharField(max_length=256, null=True)
    description = models.TextField(null=True)
    created = models.DateField(auto_now_add=True)
    updated = models.DateField(auto_now=True)

    def __str__(self):
        return self.name

class Client(Person):
    abbreviation = models.CharField(max_length=5)
    company = models.ForeignKey(Company,on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return "%s %s" % (self.firstname, self.lastname)

class Project(models.Model):
    client = models.ForeignKey(Client,on_delete=models.SET_NULL, null=True)
    company = models.ForeignKey(Company,on_delete=models.SET_NULL, null=True)
    default_vimeo_passwd = models.CharField(max_length=100, default="pwd")
    projectmanager = models.ManyToManyField(Projectmanager)
    name = models.CharField(max_length=256, unique=True)
    abbreviation = models.CharField(max_length=10, unique=True)
    change_log = models.TextField()
    video_target_width = models.IntegerField(default='1280')
    video_target_height = models.IntegerField(default='720')
    video_target_size = models.IntegerField(default='2')
    video_target_duration = models.DurationField(default=timedelta(seconds=1))
    created = models.DateField(auto_now_add=True)
    updated = models.DateField(auto_now=True)

    def __str__(self):
        return self.name

class Video(models.Model):
    STATUS = (
            ('PENDING', 'PENDING'),
            ('ACTIVE', 'ACTIVE'),
            ('FAILED', 'FAILED'),
            ('COMPLETE', 'COMPLETE'),
            )

    project = models.ForeignKey(Project, on_delete=models.SET_NULL, null=True,related_name="videos")
    title = models.CharField(max_length=100)
    description = models.TextField(null=True)
    version = models.CharField(max_length=25)
    vimeo_passwd = models.CharField(max_length=100)
    url = models.TextField()
    url_review = models.TextField()
    url_download = models.TextField()
    status = models.CharField(max_length=8, choices=STATUS, default='PENDING')
    unique_fn = models.CharField(max_length=255)
    ffprobe_result = JSONField(null=True)
    response_upload = JSONField(null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ["title"]
   

    def __str__(self):
        return '%s,%s,%s,%s,%s,%s' % (self.title, self.version, self.vimeo_passwd, self.url, self.status, self.unique_fn)

class VimeoResponse(models.Model):
    video = models.OneToOneField(
            Video,
            on_delete=models.CASCADE,
            primary_key=True
            )
    endpoint = models.URLField(max_length=255)
    response_text = JSONField(null=True)
