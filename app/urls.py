from django.urls import path

from app import views

app_name = 'app'
urlpatterns = [
    path('', views.index, name="index"),
    path('admin/clipboardinfo/<int:project_id>/', views.clipboard_info, name="clipboardinfo"),
    path('admin/lastnvideos/', views.lastnvideos, name="lastnvideos"),
        ]


