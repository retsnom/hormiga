from django.utils.safestring import mark_safe
from django.contrib import admin
from .models import Company, Client, Projectmanager, Project, Video, VimeoResponse
from django.forms.models import BaseInlineFormSet
from nested_inline.admin import NestedTabularInline, NestedStackedInline, NestedModelAdmin
admin.site.site_header = 'Hormiga admin'
admin.site.site_title = 'Hormiga admin'

class ProjectProjectmanagerInline(admin.TabularInline):
    model = Project.projectmanager.through
    extra = 0 

class ProjectVideoInline(NestedTabularInline):
    model = Video
    fields = ['title','status','version','description','vimeo_passwd','unique_fn',('url'),'created','updated']
    readonly_fields = ('unique_fn','description','vimeo_passwd','title','status','version','url','created','updated')
    can_delete = False
    extra = 0
    
class VimeoResponseInline(admin.StackedInline):
    model = VimeoResponse

class VideoInline(NestedTabularInline):
    model = Video
    extra = 0 
    fields = ['_appendend_urls','_linked_title','status','version','description','vimeo_passwd','unique_fn','url','url_review','url_download','created','updated']
    readonly_fields = ('_appendend_urls','_linked_title','unique_fn','description','vimeo_passwd','title','status','version','url','url_review','url_download','created','updated')
    ordering = ['created']    
    can_delete = False
    
    def _appendend_urls(self, obj):
        return ('URL: %s &#13;Review-URL: %s &#13;Passwort: %s &#13;Download-Link: %s') % (obj.url, obj.url_review, obj.vimeo_passwd, obj.url_download)
    @mark_safe
    def _linked_title(self, obj):
        return '<a href="/admin/app/project/'+str(obj.project.id)+'/change/#videos">%s</a>' % obj.title
    _linked_title.short_description = "Title"
    _appendend_urls.short_description = "Copy clipboard"

class ProjectClientInline(NestedTabularInline):
    model = Project
    extra = 0
    inlines = [VideoInline,]

class CompanyClientInline(NestedTabularInline):
    model = Project 
    extra = 0
    fields = ['_get_linked_name','_get_linked_client']
    readonly_fields = ['_get_linked_name','_get_linked_client']
    
    @mark_safe
    def _get_linked_name(self, obj):
        return '<a href="/admin/app/project/'+str(obj.id)+'/change/#videos">%s</a>' % obj.name
    _get_linked_name.short_description = "Project"
    @mark_safe
    def _get_linked_client(self, obj):
        return '<a href="/admin/app/client/'+str(obj.client.id)+'/change">%s %s</a>' % (obj.client.firstname, obj.client.lastname)
    _get_linked_client.short_description = "Client"

class ProjectmanagerInline(admin.TabularInline):
    model = Projectmanager

@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    inlines = [CompanyClientInline,]

@admin.register(Projectmanager)
class ProjectmanagerAdmin(admin.ModelAdmin):
    date_hierarchy = 'created'
    inlines = [ProjectProjectmanagerInline,]

@admin.register(Client)
class ClientAdmin(NestedModelAdmin):
    list_display = ('firstname','lastname', 'email', 'created', 'updated')
    inlines = [ProjectClientInline,]

@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'abbreviation', 'company', 'created', 'updated')
    fieldsets = (
        (None, {
            'classes': 'wide',
            'fields':(('name', 'abbreviation','default_vimeo_passwd'),'change_log',),
            }),
        ('Persons', {
            'fields':('projectmanager','company','client',),
           }),
        ('Video targets', {
            'fields':(('video_target_width','video_target_height','video_target_size','video_target_duration'),),
           }),

            )
    list_filter = ('company','projectmanager','created')
    
    search_fields=['name']
    autcomplete_field = ['name']
    date_hierarchy = 'created'
    filter_horizontal = ('projectmanager',)
    inlines = [VideoInline,]
