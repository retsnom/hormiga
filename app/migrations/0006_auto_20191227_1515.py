# Generated by Django 3.0.1 on 2019-12-27 14:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0005_auto_20191225_2201'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='url_download',
            field=models.TextField(default='http://google.at'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='video',
            name='url_review',
            field=models.TextField(default='http://google.at'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='video',
            name='url',
            field=models.TextField(),
        ),
    ]
