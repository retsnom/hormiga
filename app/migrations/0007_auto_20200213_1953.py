# Generated by Django 3.0.1 on 2020-02-13 18:53

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0006_auto_20191227_1515'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Projectmanager',
            new_name='Client',
        ),
        migrations.RenameField(
            model_name='project',
            old_name='projectmanager',
            new_name='Client',
        ),
    ]
