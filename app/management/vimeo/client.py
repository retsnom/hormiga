import json
import vimeo
import time

from django.conf import settings

vimeo_ = settings.VIMEO

_client = vimeo.VimeoClient(
        token = vimeo_['auth']['TOKEN'],
        key = vimeo_['auth']['KEY'],
        secret = vimeo_['auth']['SECRET']
        )

base_url = vimeo_['base_url']
user_id = vimeo_['user_id']

class ApiHttpError(Exception):
    def __init__(self, http_status, url, method, response_text):
        self.http_status = http_status
        self.url = url
        self.method = method
        self.response_text = response_text

    def __str__(self):
        return '{} {} {} {}'.format(self.http_status, self.url, self.method, self.response_text)

def _call(path, method,  params):

    if method == 'POST':
        resp = _client.post(path,data=params)
    elif method == 'PUT': 
        resp = _client.put(path)
    else:
        resp = _client.get(path)

    if resp.status_code == 404:
        return None
    elif resp.status_code == 201:
        return True
    elif resp.status_code == 204:
        return None
    elif resp.status_code != 200:
        raise ApiHttpError(resp.status_code, path, method, resp.text) 

    return json.loads(resp.text)

def _api_get(path, **kwargs):
    return _call(path,'GET',kwargs)

def _api_post(path, **kwargs):
    return _call(path,'POST',kwargs)

def _api_put(path, **kwargs):
    return _call(path,'PUT',kwargs)

def getAllFolders():
    result = _api_get('/users/%s/projects' % user_id)
    return [{'name': d['name'] ,'uri': d['uri']} for d in result['data'] ]

def getFolderByName(name):
    result = getAllFolders()

    for f in result:
        if f['name'] == name:
            return f
    return None

def createNewFolder(name):
    if getFolderByName(name) is None:
        return _api_post('/users/%s/projects' % user_id, name=name)
    return None 

def addVideoToFolder(video_id,folder_name,create=True):
    if create:
        createNewFolder(folder_name)
    result = getFolderByName(folder_name)

    if result is None:
        raise Exception('Folder %s doesnt exist or could not been created.' % folder_name)
    _api_put(result['uri']+video_id)

def uploadVideo(file, title, description, password):
    uri = _client.upload(file, 
        data={                                                                                                            
            'name': title,                                                                                    
            'description': description,
            'privacy': {                                                                                                                            
                'view': 'password',
                'download': True
                },                                                                                                                                  
            'password': password,                                                                                                         
            'review_page': {
                'active': True
                }
            }
        ) 
    return uri

def getVideoByUri(uri):
    return _api_get(uri)

def getVideoLink(data):
    if 'link' in data:
        return data['link']
    return None

def getReviewLink(data):
    if 'review_page' in data:
        if 'link' in data['review_page']:
            return data['review_page']['link']
    return None

def getDownloadLink(videoid):
    data = _api_get(videoid)
    if 'files' in data:
        for d in data['files']:
            if 'quality' in d:
                if d['quality'] == "hd":
                    return d['link']
    time.sleep(10)
    return getDownloadLink(videoid)