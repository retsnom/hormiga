#! /bin/bash
mkdir -p /code/logs
touch /code/logs/gunicorn.log
touch /code/logs/gunicorn-access.log
tail -n 0 -f /code/logs/gunicorn*.log &

gunicorn -b :8000 --chdi /code web.wsgi --log-file=/code/logs/gunicorn.log --access-logfile=/code/logs/gunicorn-access.log
/usr/local/bin/python /code/manage.py rqworker

/bin/bash
