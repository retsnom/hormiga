## Create the vimeo app

First create a vimeo app [Apps](https://developer.vimeo.com/apps) for your vimeo pro account
([see](https://developer.vimeo.com/api/guides/start)) .

## Configure your app
	1. Enable upload Access - needs approval! can take up to 5 days :(   
	2. Click Authenticated (you)
	3. Select Private, Create, Edit, Delete, Interact and if possible Upload for the token scopes
	4. You will need the the generated token, the Client identifier (key) and the Client secret (secret) for the authentication
	5. open your .env file and set the tree variables 

## Requirements

Install docker ([see the docker guide](https://www.docker.com/get-started)) .

Next start the docker demon and build the image

## Build
```
docker build -t app docker/
```
## Start the app

```
docker run -d -u 1000 -v  /path/to/project/redis-data:/code/redis-data --env-file=/path/to/project/.env --name=redis redis
docker run -d -u 1000 -v /path/to/project/postgres-data:/code/postgres-data --env-file=/path/to/project/.env --name=postgres postgres
docker run --name app -p 80:8000 -dit --rm --privileged -v /path/to/mountpoint:/files -v /path/to/project:/code --link redis:redis --link postgres:postgres app
docker run --name worker -dit --rm --privileged -v /path/to/mountpoint:/files -v /path/to/project:/code --link redis:redis --link postgres:postgres app /usr/local/bin/python /code/manage.py rqworker default &

```
